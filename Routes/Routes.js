const express=require(`express`);
const {
	updateTask,
	getTask
}=require(`./../controllers/taskControllers`)
const router = express.Router()



router.get(`/`, async (req, res)=> {
	console.log(req.body.name)
		try{
			 await getTask(req.body.name).then(results => res.send(results))
		}catch(err){
			res.status(500).json(err.message)	
		}
	})

router.put(`/:id/complete`, async(req,res)=>{
	await updateTask().then(response => res.send(response))
})


module.exports=router;
