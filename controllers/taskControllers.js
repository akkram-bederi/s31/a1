const Task=require(`./../models/Tasks`)


let newTask= new Task({
	name:"sleep",
	status:"incomplete"
})

newTask.save().then((savedTask,err) =>{
	// console.log(savedTask)
	if(savedTask){
		return savedTask
	}else{
		return err
	}
})

module.exports.getTask= async(name)=>{
	console.log(name)
		return await Task.findOne({name:name}).then((results,err)=>{
		if(results){
			return results		
		} 
		else{
			return err
		}
	})
}

module.exports.updateTask=async() =>{
	
return await Task.findOneAndUpdate({status:"incomplete"},{$set:{status:"complete"}},{new:true}).then((results,err)=>{
		if(results){
			return results		
		} 
		else{
			return err
		}
	})
}